#include <iostream>
#include "upc.h"  
using namespace std;

using uint = unsigned int;
using lli = long long int;
void printxy(uint x, int y, string txt);
void mostrarMenu();
void mostrarReglas();
void mostrarCreditos();
void mostrarError();
void mostrarJuego();
void printFrame(uint x, uint y, uint width, uint height);
void esperarTecla(char teclaEsperada);

const uint MAX_COLUMNS = 120;
const uint MAX_ROWS = 30;
const string TL = "\xC9";
const string TR = "\xBB";
const string BL = "\xC8";
const string BR = "\xBC";
const string HO = "\xCD";
const string VE = "\xBA";

int main() {
    resetAll();
    noecho();
    hideCursor();

    int opcion;

    do {
        mostrarMenu();
        opcion = _getch();
        if (opcion < '0' || opcion > '4') {
            mostrarError();
        }
        else {
            switch (opcion) {
            case '1':
                mostrarJuego();
                break;
            case '2':
                mostrarReglas();
                break;
            case '3':
                mostrarCreditos();
                break;
            case '4':
                break;
            }
        }
    } while (opcion != '0');

    return 0;
}

void Helicoptero() {
    clear();
    cout << "____________\n";
    cout << "____________\n";
    cout << "     _I_\n";
    cout << "  (o__)---- - O\n";
    cout << "__ / _ / \n";
}

void Avion() {
    clear();
    cout << "    _____  __\n";
    cout << "  _______  | \n";
    cout << "_________  |MK\\____________-====- --..._\n";
    cout << "          '-----------------.._____...-'\n";
}

void Carro() {
    cout << "       i     ______";
    cout << "       |   /  |    |";
    cout << "  _____|_/____|____|  \\ ";
    cout << " /      |    -|        | '";
    cout << "{_______|_____|_______/= ";
    cout << "   \\__/         \\__/    ";
}

void Moto() {
    cout << "        (_)";
    cout << "       / \\ ";
    cout << "  `== / /\\=,_ ";
    cout << "  ;--==\\  \\o ";
    cout << "  /____//__/__\\ ";
    cout << "  @=`(0)     (0) ";
}

void Tanque() {
    clear();
    cout << "       ____\n";
    cout << "  ____/\\__\\_____   _____  ___    ___\n";
    cout << "|____/\\  |_____/  /  /  \\/  /\\  /  /\\\n";
    cout << "|____/  \\|____/  /__/\\____/  \\/  /  \\\n";
    cout << "         |___|        \\   \\      \\   \\\n";
    cout << "                    \\___\\     \\___\\\n";
}

void Bicicleta() {
    cout << "              __";
    cout << "   ,--.      <__)";
    cout << "   `- |________7";
    cout << "     |`.      | ";
    cout << "   .--|. \\     |.\\--";
    cout << "  /   j \\ `.7__j__\\  ";
    cout << " |   o   | (o)____O)  |";
    cout << "  \\     /   J  \\     /";
    cout << "   `---'        `---' ";
}

void Camion() {

    cout << " o_______________}o{";
    cout << "   |              |    ";
    cout << "   |    911       |____\\_____";
    cout << "   | _____        |    |_o__ |";
    cout << "    [/ ___ \\       |   / ___ \\|";
    cout << "    []_/.-.\\_\\______|__/_/.-.\\_[]";
    cout << "       |(O)|             |(O)|";
    cout << "        '-'               '-'";
}

void Mototaxi() {
    clear();
    cout << "Aun no desbloqueas este vehiculo :( \n";
}

// Mueve el cursor a x;y -> Luego imprime
void printxy(uint x, int y, string txt) {
    gotoxy(x, y);
    cout << txt;
}

// Marco
void printFrame(uint x, uint y, uint width, uint height) {
    // Imprime Esquinas
    printxy(x, y, TL);
    printxy(x + width - 1, y, TR);
    printxy(x, y + height - 1, BL);
    printxy(x + width - 1, y + height - 1, BR);

    // Imprime las l�neas horizontales y verticales
    for (int i = 1; i < width - 1; ++i) {
        printxy(x + i, y, HO);
        printxy(x + i, y + height - 1, HO);
    }

    for (int i = 1; i < height - 1; ++i) {
        printxy(x, y + i, VE);
        printxy(x + width - 1, y + i, VE);
    }
}

void mostrarMenu() {
    clear();

    uint width = 72;
    uint height = 22;
    uint left = MAX_COLUMNS / 2 - width / 2;
    uint top = MAX_ROWS / 2 - height / 2;

    color(DARK_RED);

    printFrame(left, top, width, height);

    printxy(left + 3, top + 2, "                                          ,--.                      ");
    printxy(left + 3, top + 3, " ,---.   ,--,--. ,--,--,--.  ,---.        `--'  ,--,--. ,--,--,--. ");
    printxy(left + 3, top + 4, "| .-. | ' ,-.  | |        | | .-. :       ,--. ' ,-.  | |        |  ");
    printxy(left + 3, top + 5, "' '-' ' \\ '-'  | |  |  |  | \\   --.       |  | \\ '-'  | |  |  |  | ");
    printxy(left + 3, top + 6, ".`-  /   `--`--' `--`--`--'  `----'     .-'  /  `--`--' `--`--`--'  ");
    printxy(left + 3, top + 7, "`---'                                   '---'                      ");

    color(WHITE);
    printxy(left + 23, top + 11, "[1]. Jugar");
    printxy(left + 23, top + 12, "[2]. Reglas");
    printxy(left + 23, top + 13, "[3]. Creditos");
    printxy(left + 23, top + 14, "[0]. Salir");
    printxy(left + 23, top + 16, "Escoja una opcion [0..3]");
}

// Funci�n para esperar a una tecla espec�fica
void esperarTecla(char teclaEsperada) {
    int regresar;
    do {
        regresar = _getch();
    } while (regresar != teclaEsperada);
}

void mostrarReglas() {
    clear();

    uint width = 72;
    uint height = 26;
    uint left = MAX_COLUMNS / 2 - width / 2;
    uint top = MAX_ROWS / 2 - height / 2;

    color(DARK_BLUE);

    printFrame(left, top, width, height);

    printxy(left + 19, top + 2, "                 _           ");
    printxy(left + 19, top + 3, "                | |          ");
    printxy(left + 19, top + 4, "  _ __ ___  __ _| | __ _ ___ ");
    printxy(left + 19, top + 5, " | '__/ _ \\/ _` | |/ _` / __|");
    printxy(left + 19, top + 6, " |_|  \\___|\\__, |_|\\__,_|___/");
    printxy(left + 19, top + 7, "            __/ |            ");
    printxy(left + 19, top + 8, "           |___/             ");
    color(DARK_CYAN);

    printxy(left + 3, top + 10, "1. El juego consta de rescatar 10 rehenes.");
    printxy(left + 3, top + 11, "2. Escoge uno de los 3 vehiculos(auto, moto, avion).");
    printxy(left + 3, top + 12, "3. Solo podras llevar uno por uno.");
    printxy(left + 3, top + 13, "4. Llevalos a la zona segura.");
    printxy(left + 3, top + 14, "5. Ten cuidado, hay enemigos esparcidos por el mapa.");
    printxy(left + 3, top + 15, "6. Ganas la partida si rescatas a todos los rehenes.");
    printxy(left + 32, top + 19, "Suerte");
    printxy(left + 3, top + 24, "Presiona [9] para volver...");

    esperarTecla('9');
}

void mostrarCreditos() {
    clear();
    color(DARK_YELLOW);

    uint width = 72;
    uint height = 26;
    uint left = MAX_COLUMNS / 2 - width / 2;
    uint top = MAX_ROWS / 2 - height / 2;

    printFrame(left, top, width, height);

    printxy(left + 16, top + 1, " _____              _ _ _            ");
    printxy(left + 16, top + 2, "/  __ \\            | (_) |           ");
    printxy(left + 16, top + 3, "| /  \\/_ __ ___  __| |_| |_ ___  ___ ");
    printxy(left + 16, top + 4, "| |   | '__/ _ \\/ _` | | __/ _ \\/ __|");
    printxy(left + 16, top + 5, "| \\__/\\ | |  __/ (_| | | || (_) \\__ \\ ");
    printxy(left + 16, top + 6, " \\____/_|  \\___|\\__,_|_|\\__\\___/|___/");

    printxy(left + 26, top + 13, "- Fabrizio Cueva");
    printxy(left + 26, top + 14, "- Carlos Cespedes");

    printxy(left + 4, top + 24, "Presiona [9] para volver...");

    esperarTecla('9');
}

void mostrarError() {
    clear();
    color(DARK_RED);

    uint width = 72;
    uint height = 22;
    uint left = MAX_COLUMNS / 2 - width / 2;
    uint top = MAX_ROWS / 2 - height / 2;

    printxy(left + 19, top + 2, " ______ _____  _____   ____  _____  ");
    printxy(left + 19, top + 3, "|  ____|  __ \\|  __ \\ / __ \\|  __ \\");
    printxy(left + 19, top + 4, "| |__  | |__) | |__) | |  | | |__) |");
    printxy(left + 19, top + 5, "|  __| |  _  /|  _  /| |  | |  _  / ");
    printxy(left + 19, top + 6, "| |____| | \\ \\| | \\ \\| |__| | | \\ \\ ");
    printxy(left + 19, top + 7, "|______|_|  \\_\\_|  \\_ \\____/|_|  \\_\\");
    printxy(left + 5, top + 10, "Has presionado un digito erroneo. Vuelve al menu con el boton: [9]");

    esperarTecla('9');
}

void mostrarJuego() {
    clear();

    int choice;
    uint width = 72;
    uint height = 22;
    uint left = MAX_COLUMNS / 2 - width / 2;
    uint top = MAX_ROWS / 2 - height / 2;

    color(DARK_MAGENTA);

    printFrame(left, top, width, height);

    printxy(left + 15, top + 3, "           _     _            _           ");
    printxy(left + 15, top + 3, "__   _____| |__ (_) ___ _   _| | ___  ___ ");
    printxy(left + 15, top + 4, "\\ \\ / / _ \\ '_ \\| |/ __| | | | |/ _ \\/ __|");
    printxy(left + 15, top + 5, " \\ V /  __/ | | | | (__| |_| | | (_) \\__ \\");
    printxy(left + 15, top + 6, "  \\_/ \\___|_| |_|_|\\___|\\__,_|_|\\___/|___/");

    printxy(left + 3, top + 10, "1.-HELICOPTERO");
    printxy(left + 3, top + 11, "2.-AVION");
    printxy(left + 3, top + 12, "3.-CARRO");
    printxy(left + 3, top + 13, "4.-MOTO");
    printxy(left + 3, top + 14, "5.-TANQUE");
    printxy(left + 3, top + 15, "6.-BICICLETA");
    printxy(left + 3, top + 16, "7.-CAMION");
    printxy(left + 3, top + 17, "8.-MOTOTAXI");
    printxy(left + 3, top + 19, "Seleccione un numero para elegir su transporte...");


    choice = _getch() - '0';  

    switch (choice) {
    case 1:
        Helicoptero();
        break;
    case 2:
        Avion();
        break;
    case 3:
        Carro();
        break;
    case 4:
        Moto();
        break;
    case 5:
        Tanque();
        break;
    case 6:
        Bicicleta();
        break;
    case 7:
        Camion();
        break;
    case 8:
        Mototaxi();
        break;
    default:
        cout << "Opcion no valida.\n";
        break;
    }

    printxy(left + 3, top + 22, "Presiona [9] para volver...");
    esperarTecla('9');
}

